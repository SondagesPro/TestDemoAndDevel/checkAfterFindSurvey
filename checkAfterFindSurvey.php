<?php

class checkAfterFindSurvey extends PluginBase
{
    static protected $description = 'Test plugin';
    static protected $name = 'checkAfterFindSurvey';

    private $count= 0;

    public function init()
    {
        $this->subscribe("afterFindSurvey");
    }
    public function afterFindSurvey()
    {
        $this->count++;
        tracevar("afterFindSurvey happen : ".$this->count);
    }
}
